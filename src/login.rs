use std::collections::HashMap;

use color_eyre::Report;
use openssl::{
    bn::BigNum,
    rsa::{Padding, RsaPrivateKeyBuilder},
};
use scraper::{Html, Selector};
use serde::Deserialize;

// Get global auth cache
const GLOBAL_AUTH_GETPUBKEY_URL: &str =
    "https://zjuam.zju.edu.cn/cas/v2/getPubKey";
const GLOBAL_AUTH_LOGIN_URL: &str = "https://zjuam.zju.edu.cn/cas/login";
const INTERMEDIATE_FORM_URL: &str =
    "http://mapp.zju.edu.cn/_web/_lightapp/pay/api/code.rst";
const AUTH_PAYCODE_LOCAL_URL: &str =
    "http://ecardpay.zju.edu.cn:9001/ThirdWeb/AuthPayCodeLocal";

#[derive(Deserialize, Debug)]
struct PubKey {
    modulus: String,
    exponent: String,
}

pub async fn get_login_state(
    client: &reqwest::Client,
    username: &str,
    password: &str,
) -> Result<(), Report> {
    let pubkey: PubKey = client
        .get(GLOBAL_AUTH_GETPUBKEY_URL)
        .send()
        .await?
        .json()
        .await?;
    let document = Html::parse_document(
        &client
            .get(GLOBAL_AUTH_LOGIN_URL)
            .send()
            .await?
            .text()
            .await?,
    );
    let mut postdata = get_default_post_data(document)?;

    let n = BigNum::from_hex_str(&pubkey.modulus).unwrap();
    let e = BigNum::from_hex_str(&pubkey.exponent).unwrap();
    let d = BigNum::new().unwrap();

    let privkey = RsaPrivateKeyBuilder::new(n, e, d)?.build();
    let padded_password =
        get_padded_password(password, pubkey.modulus.len() / 2);
    let mut encrypted_password = vec![0; 64];

    privkey.public_encrypt(
        padded_password.as_bytes(),
        &mut encrypted_password,
        Padding::NONE,
    )?;

    let password = hex::encode(encrypted_password);
    postdata.insert("username".to_owned(), username.to_owned());
    postdata.insert("password".to_owned(), password);

    if let Err(msg) = client
        .post(GLOBAL_AUTH_LOGIN_URL)
        .form(&postdata)
        .send()
        .await
    {
        log::warn!("{}", msg);
    }

    Ok(())
}

pub async fn get_asp_net_session_id(
    client: &reqwest::Client,
) -> Result<(), Report> {
    // IMPORTANT!
    client.get(INTERMEDIATE_FORM_URL).send().await?;

    let random_imei_code_post_data = [("code", "0")];
    let form = &client
        .post(INTERMEDIATE_FORM_URL)
        .form(&random_imei_code_post_data)
        .send()
        .await?
        .text()
        .await?;

    let document = Html::parse_document(&form);

    let mut post_data: Vec<(String, String)> = Vec::new();
    let input_sel = Selector::parse("input").unwrap();
    for input_elem in document.select(&input_sel) {
        let name = input_elem.value().attr("name").unwrap_or_default();
        if name.is_empty() {
            continue;
        } else {
            post_data.push((
                name.to_owned(),
                input_elem
                    .value()
                    .attr("value")
                    .unwrap_or_default()
                    .to_owned(),
            ));
        }
    }

    // This request should set cookie "ASP.NET_SessionId"
    client
        .post(AUTH_PAYCODE_LOCAL_URL)
        .form(&post_data)
        .send()
        .await?;

    Ok(())
}

/// Get those fields that are neither `username` nor `password`
fn get_default_post_data(
    document: Html,
) -> Result<HashMap<String, String>, Report> {
    let mut ret: HashMap<String, String> = HashMap::new();

    let input_sel = Selector::parse("input").unwrap();
    for input_elem in document.select(&input_sel) {
        let attr = input_elem.value().attr("name").unwrap_or_default();
        if attr.is_empty() || attr == "username" || attr == "password" {
            continue;
        } else {
            ret.insert(
                attr.to_owned(),
                input_elem
                    .value()
                    .attr("value")
                    .unwrap_or_default()
                    .to_owned(),
            );
        }
    }

    Ok(ret)
}

/// I have read the function `RSAUtils.encryptedString()` in the file
/// <https://zjuam.zju.edu.cn/cas/js/login/security.js>, and tried the
/// below 4 padding forms (**all of them have the same length of 64 hex
/// digits**):
///      1. "<reversed password><padded '\0' characters>"
///      2. "<password><padded '\0' characters>"
///      3. "<padded '\0' characters><reversed password>"
///      4. "<padded '\0' characters><password>"
/// Apparently the 4-th thing works.
fn get_padded_password(password: &str, final_length: usize) -> String {
    let mut nul_head = String::new();
    for _ in 0..(final_length - password.len()) {
        nul_head.push('\0');
    }
    let mut ret = nul_head;
    ret.push_str(password);

    ret
}

#[cfg(test)]
mod login_function {
    use color_eyre::Report;
    use openssl::{
        bn::BigNum,
        rsa::{Padding, RsaPrivateKeyBuilder},
    };

    use super::get_padded_password;

    #[test]
    /// Values in this test are set according to a debugging process in chrome
    fn is_encryption_correctly_implemented() -> Result<(), Report> {
        let password = "b2106919ab5e1c";
        let modulus = "ebd2304e2e68bf2449e76b683f7d2669a6b37ec68a77b2ffea86b7f8b1aa1d2f7ca6f335f12b514c1899e75386a79a4cb78815a8e84fd699737f8b5ce33824ed";
        let exponent = "10001";

        let n = BigNum::from_hex_str(modulus)?;
        let e = BigNum::from_hex_str(exponent)?;
        let d = BigNum::new()?;
        let privkey = RsaPrivateKeyBuilder::new(n, e, d)?.build();

        let padded_password =
            get_padded_password(password, modulus.len() / 2);
        let mut encrypted_password = vec![0; 64];
        privkey.public_encrypt(
            padded_password.as_bytes(),
            &mut encrypted_password,
            Padding::NONE,
        )?;

        let encrypted_password = hex::encode(encrypted_password);
        assert_eq!("3370b56e02c56b2a6ef6c32d01dd1a847bb277398318fa60f69fcbec141e81858211511cc2ff560f4b3caf3ff692b262ac044590f96be58759685c0e1bd4916b", encrypted_password);

        Ok(())
    }
}

// Author: Blurgy <gy@blurgy.xyz>
// Date:   Oct 12 2021, 23:50 [CST]
